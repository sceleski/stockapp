'use strict';

/**
 * @ngdoc directive
 * @name capsotestApp.directive:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the capsotestApp
 */
angular.module('capsotestApp')
  .directive('chart', function () {
    return {
        restrict: 'A',
        scope: {
        	prices: '=prices',
        	company: '@'
        },
        link: function($scope, element, attrs) {

		  var chartOptions = {
		    ///Boolean - Whether grid lines are shown across the chart
		    scaleShowGridLines : true,
		    //String - Colour of the grid lines
		    scaleGridLineColor : 'rgba(0,0,0,.05)',
		    //Number - Width of the grid lines
		    scaleGridLineWidth : 1,
		    //Boolean - Whether to show horizontal lines (except X axis)
		    scaleShowHorizontalLines: true,
		    //Boolean - Whether to show vertical lines (except Y axis)
		    scaleShowVerticalLines: false,
		    //Boolean - Whether the line is curved between points
		    bezierCurve : true,
		    //Number - Tension of the bezier curve between points
		    bezierCurveTension : 0,
		    //Boolean - Whether to show a dot for each point
		    pointDot : false,
		    //Number - Radius of each point dot in pixels
		    pointDotRadius : 2,
		    //Number - Pixel width of point dot stroke
		    pointDotStrokeWidth : 1,
		    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
		    pointHitDetectionRadius : 10,
		    //Boolean - Whether to show a stroke for datasets
		    datasetStroke : true,
		    //Number - Pixel width of dataset stroke
		    datasetStrokeWidth : 3,
		    //Boolean - Whether to fill the dataset with a colour
		    datasetFill : true
		  };

  	  	  var labels = [];
  	  	  var prices = [];

	  	  angular.forEach($scope.prices, function(value,key) {
  	  	  	if(key%190==0) {
  	  	  	  var price = value.price.toFixed(2);
  	  	  	  prices.push(price);
  	  	  	  if(key%190==0) {
  	  	  	    var timestamp = new Date(value.timestamp);
  	  	  	    var month     = timestamp.getMonth()+1;
  	  	  	    var year      = timestamp.getFullYear();
  	  	  	    var label     = month + '/' + year;
  	  	  	    labels.push(label);
  	  	  	  } else {
  	  	  	  	labels.push('');
  	  	  	  }
  	  	  	}
	  	  });

		  var chartData = {
		    labels: labels,
		      datasets: [
		        {
		          label: 'Prices',
		          fillColor: 'rgba(151,187,205,0.2)',
		          strokeColor: 'rgba(151,187,205,1)',
		          pointColor: 'rgba(151,187,205,1)',
		          pointStrokeColor: '#fff',
		          pointHighlightFill: '#fff',
		          pointHighlightStroke: 'rgba(151,187,205,1)',
		          data: prices
		        }
		    ]
		  };

	  	  var ctx = element.context.getContext('2d');
	  	  new Chart(ctx).Line(chartData,chartOptions);

        }
    }
  });
  