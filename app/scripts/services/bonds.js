'use strict';

/**
 * @ngdoc service
 * @name capsotestApp.service:BondsService
 * @description
 * # BondsService
 * Service of the Bonds
 */
angular.module('capsotestApp')
  .factory('BondsService', ['$http', function($http) {
    var Service = {};
  
    /**
     * Get list of the bonds
     */
    Service.getBonds = function() {
      return $http.get('https://s3-eu-west-1.amazonaws.com/capso.test.data/bondmaster.json');
    };
  
    return Service;
  }]);
