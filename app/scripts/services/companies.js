'use strict';

/**
 * @ngdoc service
 * @name capsotestApp.service:CompaniesService
 * @description
 * # CompaniesService
 * Service of the Companies
 */
angular.module('capsotestApp')
  .factory('CompaniesService', ['$http', function($http) {
    var Service = {};
  
    /**
     * Get list of the companies
     */
    Service.getCompanies = function() {
      return $http.get('https://s3-eu-west-1.amazonaws.com/capso.test.data/companies.json');
    };
  
    return Service;
  }]);
