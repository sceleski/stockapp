'use strict';

/**
 * @ngdoc function
 * @name capsotestApp.controller:BondsCtrl
 * @description
 * # BondsCtrl
 * Controller of the Bonds
 */
angular.module('capsotestApp')
  .controller('BondsCtrl', function ($scope, BondsService, $stateParams, CompaniesService) {

  	$scope.bonds = [];
  	BondsService.getBonds().success(function(data){
  	  angular.forEach(data, function(value,key) {
  	  	if(value.issuerId==$stateParams.id) {
  	  	  $scope.bonds.push(value);
  	  	};
  	  });
    });

  	$scope.company = {};
  	CompaniesService.getCompanies().success(function(data){
  	  angular.forEach(data.results, function(value,key) {
  	  	if(value.companyId==$stateParams.id) {
  	  	  $scope.company = value;
  	  	}
  	  });
    });

  });
