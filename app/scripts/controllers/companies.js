'use strict';

/**
 * @ngdoc function
 * @name capsotestApp.controller:CompaniesCtrl
 * @description
 * # CompaniesCtrl
 * Controller of the Companies
 */
angular.module('capsotestApp')
  .controller('CompaniesCtrl', function ($scope, CompaniesService) {
  	$scope.companies = [];
  	CompaniesService.getCompanies().success(function(data){
      $scope.companies = data.results;
    });
  });
